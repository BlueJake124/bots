import discord
from discord.ext import commands
import random
bot = commands.Bot(command_prefix='$')
cute_replies = ['nah', 'nope', 'Nuuu', "o////o", 'Nu x3', 'nope, you are!', 'Am not cute!!', "nu *shy*"]

@bot.event
async def on_ready():
    channel = bot.get_channel(908742999691919380) # channel: bot-status
    print(f"We have logged succsesfully as {bot.user}")
    await channel.send("Currently online and ready to roll!!")

@bot.command()
async def praise(ctx, target):
    '''
    Praises a given user.
    $praise <user @>
    '''
    await ctx.send(f'Prasie the allmighty {target}!! *bows*')

@bot.command()
async def dom(ctx, target=None):
    '''
    Get the dom % of you or a user.
    $dom optinal:<user @>
    '''
    if target == None:
        await ctx.send(f"You are {random.randint(0, 100)}% a dom!")
    else:
        await ctx.send(f"{target} is {random.randint(0, 100)}% a dom!")
        
@bot.command()
async def sub(ctx, target=None):
    '''
    Get the sub % of you or a user.
    $dom optinal:<user @>
    '''
    if target == None:
        await ctx.send(f"Your {random.randint(0, 100)}% a sub!")
    else:
        await ctx.send(f'{target} is {random.randint(0, 100)}% a sub!')

@bot.command()
async def iq(ctx, target=None):
    '''
    IQ level of you or a given user.
    $iq optinal:<usere @>
    '''
    if target == None:
        await ctx.send(f"Your IQ is {random.randint(-10, 400)}!")
    else:
        await ctx.send(f'{target}\'s IQ is {random.randint(-10, 400)}!')

@bot.command()
async def flip(ctx):
    '''
    Flip a coin!
    '''
    chance = random.randint(0,1)
    if chance == 1:
        await ctx.send('Heads!!')
    else:
        await ctx.send("Tails!!")

@bot.command()
async def flop(ctx, target=None):
    '''
    Make me flop, or flop on a specified user!
    $flop optinal:<user @>
    '''
    if target != None:
        await ctx.send(f'*flops on {target}* mmm nice and soft ~~')
    else:
        await ctx.send(f'*flops* <~ w~>')

@bot.command()
async def father(ctx):
    '''
    yes
    '''
    await ctx.send("My father is @Verve UwU")

@bot.command()
async def cute(ctx, target=None):
    '''
    Ask if im cute, or a specified user!
    $cute optinal:<user @>
    '''
    if target != None:
        if random.randint(0,1) == 1:
            await ctx.send(f'{target} is cute!!!! *pats*')
        else:
            await ctx.send(f"Eh, {target} aint that cute >.>")
    else:
        await ctx.send(random.choice(cute_replies))

@bot.command()
async def snuggle(ctx, target):
    '''
    Snuggles a  targeted user!
    $snuggle <target @>
    '''
    await ctx.send(f'Awww~~ you snuggle {target} with love and effection UwU')

@bot.command()
async def cuddle(ctx, target):
    '''
    Cuddles a  targeted user!
    $cuddle <target @>
    '''
    await ctx.send(f'Aww, You cuddle {target} softly~ UwU')

@bot.command()
async def pp(ctx, target = None):
    '''
    pp size of your or a user!
    $pp optinal:<user @>
    '''
    x = ['=' for i in range(random.randint(0, 50))] # gen
    x[0] = 'B'
    x[len(x)] = 'D'
    string = ''.join(x)
    if target == None:
        await ctx.send(f'Your pp size is {string}')
    else:
        await ctx.send(f'{target}\'s pp size is {string}')

        

print('Logging in...')
bot.run('OTA4NzQxMTI5NDIyMzg5Mjk4.YY6JIA.XwGzLBLf_1sbSb-FKJo7yUAi24Y')